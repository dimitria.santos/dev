package br.edu.cest.proj02;

import java.util.Scanner;

public class ParImpar {

	private static Scanner in;

	public static void main(String[] args) {
		int a, b;
	    in = new Scanner(System.in);
	        
	    System.out.print("Digite um valor inteiro: ");
	    a = in.nextInt();
	    b = (a % 2);
	        
	    if (b==0){
	    	System.out.printf("O número %d é PAR",a);
	    	
	    }else{
	    	System.out.printf("O número %d é IMPAR",a);
	    }

	}

}
