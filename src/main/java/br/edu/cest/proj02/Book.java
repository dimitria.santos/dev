package br.edu.cest.proj02;

public class Book {
    public String author;
    public int year;
    public String publisher;
    public int ISBN;
    public int price;
    
    public Book (int ISBN, String author,int year,String publisher,int price){  
    	
        this.author=author;
        this.year=year;
        this.publisher=publisher;
        this.ISBN=ISBN;
        this.price=price;
    }
    public void getauthor() {
    	System.out.println("Esse livro é do author: "+author);
    }
    public void getYear() {
    	System.out.println("Esse livro é do ano: "+year);
    }
    public void publisher() {
    	System.out.println("A edição é: "+publisher);
    }
    public void getISBN() {
    	System.out.println("A identificação do livro é: "+ISBN);
    }
    public void setprice() {
    	System.out.println("Preço: "+price);
    }
    
public class Item {
	public String title;
	public int volume;
	public int edition;
	
	public Item (String title, int volume, int edition ) {
		this.title=title;
		this.volume=volume;
		this.edition=edition;
	}
	public void gettitle() {
		System.out.println("O título do livro é: "+title);
	}
	public void getvolume() {
		System.out.println("O volume do livro é: "+volume);		
	}
	public void getedition() {
		System.out.println("Edição do livro: "+edition);
	}
	
	
	}

}
